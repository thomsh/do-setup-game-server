#!/usr/bin/env bash
set -euxo pipefail

# logs
install -d -m 0755 /var/log/do-setup-game-server
exec > >(tee -a "/var/log/do-setup-game-server/bootstrap.log") 2>&1
echo "$(date +%F%T%Z) - START bootstrap"

# apt & deps
export DEBIAN_FRONTEND=noninteractive
apt-get update # main update subscript don't update unless required
apt-get install -qy git

# get all scripts
if [ ! -d "/opt/do-setup-game-server" ];then
  git clone https://gitlab.com/thomsh/do-setup-game-server.git /opt/do-setup-game-server
else
  pushd /opt/do-setup-game-server
  git fetch origin master
  git reset --hard origin/master
  popd
fi
# run all tasks
cd /opt/do-setup-game-server
for task in default "$@"
do
  find "$task" -type f -iname '*.sh'|sort -n|while read -r f;do
    bash -x "$f"
  done
done

apt-get clean
echo "$(date +%F%T%Z) - END bootstrap"
exit 0

