#!/usr/bin/env bash
set -euxo pipefail
if [ -f /etc/persistent-volume-uuid ];then
  UUID="$(head -n 1 /etc/persistent-volume-uuid)"
  if ! grep '/srv' /etc/fstab;then
    echo "/dev/disk/by-uuid/${UUID} /srv ext4 defaults,nofail,discard 0 0" | tee -a /etc/fstab
  fi
  mount -a
fi

