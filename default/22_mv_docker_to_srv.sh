#!/usr/bin/env bash
set -euxo pipefail
if [ -f /etc/persistent-volume-uuid ];then
  if [ ! -d /srv/docker ];then
    systemctl stop docker.service
    mv /var/lib/docker /srv/docker
    ln -snf /srv/docker /var/lib/docker
    systemctl restart docker.service
  else
    echo "docker already in /srv"
  fi
fi
