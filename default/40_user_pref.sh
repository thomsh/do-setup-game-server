#!/usr/bin/env bash
set -euxo pipefail
cat > /root/.bashrc <<EOF
alias l='ls -alh --color=auto'
EOF

cat > /root/.vimrc <<EOF
scriptencoding utf-8
set nocompatible
set mouse-=a
set bg=dark
syntax on

set wildmenu
set showmatch
set showcmd
set incsearch           " search as characters are entered
set hlsearch            " highlight matches

set tabstop=2
set sts=2
set sw=2
" file specific ts
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType sh setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType html setlocal ts=2 sts=2 sw=2 expandtab
set expandtab
EOF
