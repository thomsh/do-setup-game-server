#!/usr/bin/env bash
set -euxo pipefail
apt-get install -qy \
    apt-transport-https \
    ca-certificates \
    curl \
    dirmngr \
    gnupg2 \
    software-properties-common

if ! grep 'https://download.docker.com/linux/debian' /etc/apt/sources.list;then
  curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
  add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
  apt-get update
  apt-get remove -qy docker docker-engine
fi
apt-get install -qy docker-ce docker-ce-cli containerd.io

# compose tool
if [ ! -f /usr/local/bin/docker-compose ];then
  curl -SfL "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose
  chmod 755 /usr/local/bin/docker-compose
fi
exit 0
