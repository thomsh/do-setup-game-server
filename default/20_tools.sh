#!/usr/bin/env bash
set -euxo pipefail

# Installing in one way lot of default tools
apt-get install -qy \
  arptables \
  bash-completion \
  bc \
  bridge-utils \
  conntrack \
  curl \
  dirmngr \
  dnsutils \
  git \
  gnupg2 \
  htop \
  iftop \
  iotop \
  iptables \
  iputils-arping \
  iputils-ping \
  jid \
  jq \
  less \
  lsof \
  lvm2 \
  lzop \
  mailutils \
  ncdu \
  netcat-openbsd \
  parted \
  pbzip2 \
  pigz \
  pv \
  pwgen \
  rsync \
  screen \
  sysstat \
  tcpdump \
  tmux \
  traceroute \
  tree \
  unzip \
  vim-nox \
  xfsprogs \
  zram-tools

exit 0
