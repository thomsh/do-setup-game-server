#!/usr/bin/env bash
set -euxo pipefail
timedatectl set-ntp true
locale-gen --purge en_US.UTF-8
echo -e 'LANG="en_US.UTF-8"\nLANGUAGE="en_US:en"\n' > /etc/default/locale
