#!/usr/bin/bash
set -euxo pipefail
docker pull thomsh/dontstarvetogether
mkdir -p /srv/dst
cd /srv/dst
TOKEN="$(cat /srv/dst/token.b64 |base64 -d)"
PASS="$(head -n 1 /srv/dst/pass)"
cat > env-basic <<EOF
TOKEN=$TOKEN
NAME=urdo-basic
PASSWORD=$PASS
OFFLINE_ENABLE=false
LAN_ONLY=false
GAME_MODE=endless
INTENTION=cooperative
TICK_RATE=30
PAUSE_WHEN_EMPTY=true
EOF

cat > run-basic.sh <<EOF
#!/bin/bash
set -euxo pipefail
docker run \\
  --restart always \\
  -it \\
  --name urdobasic \\
  -p 10999:10999/udp \\
  --env-file /srv/dst/env-basic \\
  thomsh/dontstarvetogether
EOF
