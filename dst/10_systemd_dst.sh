#!/usr/bin/env bash
set -euxo pipefail

cat > /etc/systemd/system/dst.service <<EOF
[Unit]
Description=Start a dst server configured in script
Requires=docker.service
After=docker.service

[Service]
Type=oneshot
ExecStart=/srv/dst/system-run-dst.sh
RemainAfterExit=true

[Install]
WantedBy=multi-user.target
EOF

if [ ! -f /srv/dst/system-run-dst.sh ];then
  cat > /srv/dst/system-run-dst.sh <<EOF
#!/usr/bin/env bash
set -euxo pipefail
echo edit me
exit 0
EOF
  chmod 755 /srv/dst/system-run-dst.sh
fi


systemctl daemon-reload
systemctl enable dst.service
systemctl start dst.service
